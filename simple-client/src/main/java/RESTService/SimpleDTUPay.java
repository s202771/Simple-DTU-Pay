package RESTService;

import domain.Payment;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class SimpleDTUPay {

    Client client = ClientBuilder.newClient();
    Response response;

    public Response pay(int amount, String cid, String mid){
        WebTarget target = client.target("http://localhost:8080/payments");
        Payment payment = new Payment();
        payment.setAmount(amount);
        payment.setCid(cid);
        payment.setMid(mid);
        response = target.request().post(Entity.entity(payment, MediaType.APPLICATION_JSON_TYPE));;
        return response;
    }

    public List<Payment> getPayments(){
        WebTarget target = client.target("http://localhost:8080/payments");
        Response response = target.request().get();
        List<Payment> payments = target
                .request()
                .get(new GenericType<List<Payment>>(){});
        response.close();
        return payments;
    }
    public void closeResponse(){
        response.close();
    }
}
