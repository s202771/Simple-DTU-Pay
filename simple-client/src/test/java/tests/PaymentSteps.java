package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import RESTService.SimpleDTUPay;
import domain.Payment;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import java.util.List;

public class PaymentSteps {
	String cid, mid;
	SimpleDTUPay dtuPay = new SimpleDTUPay();
	List<Payment> payments;
	BadRequestException badRequestException;
	Response response;

	boolean successful;
	@Given("a customer with id {string}")
	public void aCustomerWithId(String cid) {
		this.cid = cid;
	}
	@Given("a merchant with id {string}")
	public void aMerchantWithId(String mid) {
		this.mid = mid;
	}
	@When("the merchant initiates a payment for {int} kr by the customer")
	public void theMerchantInitiatesAPaymentForKrByTheCustomer(int amount) {
		response = dtuPay.pay(amount,cid,mid);
	}
	@Then("the payment is successful")
	public void thePaymentIsSuccessful() {
		assertTrue(response.getStatus() == 201);
	}

	@Given("a successful payment of {int} kr from customer {string} to merchant {string}")
	public void aSuccessfulPaymentOfKrFromCustomerToMerchant(int amount, String cid, String mid) {
		var response = dtuPay.pay(amount,cid,mid);
		assertTrue(response.getStatus() == 201);

	}
	@When("the manager asks for a list of payments")
	public void theManagerAsksForAListOfPayments() {
		payments = dtuPay.getPayments();
	}

	@Then("the list contains a payments where customer {string} paid {int} kr to merchant {string}")
	public void theListContainsAPaymentsWhereCustomerPaidKrToMerchant(String cid, int amount, String mid) {
		boolean result = false;

		for (Payment payment: payments){
			if (payment.getAmount() == amount && payment.getCid().equals(cid) && payment.getMid().equals(mid)){
				result = true;
				break;
			}
		}
		assertTrue(result);
	}

	@Then("the payment is not successful")
	public void thePaymentIsNotSuccessful() {
		assertTrue(response.getStatus() == 400);
	}

	@Then("an error message is returned saying {string}")
	public void anErrorMessageIsReturnedSaying(String string) {
		assertTrue(response.readEntity(String.class).equals(string));
	}

	@After
	public void closeResponse(){
		dtuPay.closeResponse();
	}
}

