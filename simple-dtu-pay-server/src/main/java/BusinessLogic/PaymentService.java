package BusinessLogic;

import Domain.Payment;

import java.util.ArrayList;

public class PaymentService {

    public static PaymentService instance = new PaymentService();
    private ArrayList<Payment> payments;

    private PaymentService() {
        this.payments = new ArrayList<>();
    }

    public void performPayment(int amount, String cid, String mid){
        var payment = new Payment();
        payment.setAmount(amount);
        payment.setCid(cid);
        payment.setMid(mid);
        payments.add(payment);
    }

    public void addPayment(Payment payment){
        this.payments.add(payment);
    }

    public Payment getPayment(int id){
        return payments.get(id - 1);
    }

    public ArrayList<Payment> getPayments() {
        return payments;
    }

    public int getPaymentId(){
        return payments.size() + 1;
    }
}
