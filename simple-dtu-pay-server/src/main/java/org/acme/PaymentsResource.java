package org.acme;

import BusinessLogic.PaymentService;
import Domain.Payment;

import javax.transaction.Status;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Path("/payments")
public class PaymentsResource {

    PaymentService paymentService = PaymentService.instance;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Payment> getPayments() {
        return paymentService.getPayments();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response pay(Payment payment) throws Exception {
        if (!payment.getCid().equals("cid1")){
            BadRequestException badRequestException = new BadRequestException(Response.status(Response.Status.BAD_REQUEST)
                    .entity("customer with id " + payment.getCid() + " is unknown").build());
            System.out.println(badRequestException.getMessage());
            throw badRequestException;
        }
        else if (!payment.getMid().equals("mid1")){
            BadRequestException badRequestException = new BadRequestException(Response.status(Response.Status.BAD_REQUEST)
                    .entity("merchant with id " + payment.getMid() + " is unknown").build());
            System.out.println(badRequestException.getMessage());
            throw badRequestException;
        }
        int id = paymentService.getPaymentId();
        payment.setId(id);
        paymentService.addPayment(payment);
        return Response.created(new URI("/payments/" + id)).build();
    }
}